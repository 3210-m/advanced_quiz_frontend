import React, {Component, Fragment} from 'react';
import {BrowserRouter as Router, Link} from 'react-router-dom'
import {Route, Switch} from "react-router";
import {Icon} from 'antd'
import addProduct from './components/addProduct'
import orders from "./components/orders";
import mall from "./components/mall";
import './App.less';
import 'antd/dist/antd.css';


class App extends Component {
    render() {
        return (
            <Fragment>
                <Router>
                    <header className='header'>
                        <div className='a-title'><Link className='title' to='/mallList'><Icon type="bank" className="icon"  />商城</Link></div>
                        <div className='a-title'><Link className='title' to='/orderList'><Icon type="shopping-cart" className="icon" />订单</Link></div>
                        <div className='a-title'><Link className='title' to='/'><Icon type="plus" className="icon"  />添加商品</Link></div>
                    </header>

                    <Switch>
                        <Route exact path='/orderList' component={orders}></Route>
                        <Route exact path='/mallList' component={mall}></Route>
                        <Route exact path='/' component={addProduct}></Route>
                    </Switch>
                </Router>
            </Fragment>
        );
    }
}

export default App;