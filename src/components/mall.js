import React from 'react';
import {getorderListAction} from "../action/getorderListAction";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import './mall.less'
import {getMallListAction} from "../action/getMallListAction";
import ProductIcon from "./ProductIcon";

class Mall extends React.Component {

    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() {
        this.props.getMallListAction();
    }

    render() {
        let data = this.props.mallList;

        return (
            <div className='mailList'>
                <ul>
                    {data.map((item, index) => {
                        return (
                            <li className='product-icon' key={index}>
                                <ProductIcon product={item}/>
                            </li>)
                    })}

                </ul>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    mallList: state.mallList.mallList
});
const mapDispatcherToProps = dispatch => bindActionCreators({
    getMallListAction
}, dispatch);
export default connect(mapStateToProps, mapDispatcherToProps)(Mall);