import React from "react";
import {message, Button} from 'antd'
import './addProduct.less'

class AddProduct extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            product : {
                productName: "",
                price: 0,
                unit: "",
                img: ""
            }
        }
    }

    addProduct(e) {
        // e.preventDefault();
        fetch('http://localhost:8080/api/addproduct', {
            method: 'POST',
            body: JSON.stringify(this.state.product),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function (response) {
            if(response.status === 201){
                message.success('创建订单成功！')
            }else {
                message.warn('创建订单失败！')
            }
        });
    }
    changeProductName(e){
        this.setState({
            product:{
                productName:e.target.value,
                price: this.state.product.price,
                unit: this.state.product.unit,
                img: this.state.product.img
            }
        })
    }
    changePrice(e){
        this.setState({
            product:{
                productName:this.state.product.productName,
                price: e.target.value,
                unit: this.state.product.unit,
                img: this.state.product.img
            }
        })
    }
    changeUnit(e){
        this.setState({
            product:{
                productName:this.state.product.productName,
                price: this.state.product.price,
                unit: e.target.value,
                img: this.state.product.img
            }
        })
    }
    changeImg(e){
        this.setState({
            product:{
                productName:this.state.product.productName,
                price: this.state.product.price,
                unit: this.state.product.unit,
                img:e.target.value
            }
        })
    }

    render() {
        return (
            <div className='addProduct'>
                <h3 className='addProductTitle'>添加商品</h3>
                <div className='form'>
                    <p>名称：</p>
                    <input id='productName' name='productName' placeholder='名称' onChange={this.changeProductName.bind(this)} />
                    <p>价格：</p>
                    <input name='price' placeholder='价格' onChange={this.changePrice.bind(this)} />
                    <p>单位：</p>
                    <input name='unit' placeholder='单位' onChange={this.changeUnit.bind(this)} />
                    <p>图片：</p>
                    <input name='img' placeholder='URL' onChange={this.changeImg.bind(this)} /><br/>
                    <Button onClick={this.addProduct.bind(this)}>提交</Button>
                </div>
            </div>
        );
    }
}


export default AddProduct;