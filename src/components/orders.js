import React from 'react';
import {getorderListAction} from "../action/getorderListAction";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {Table, Button, Empty} from 'antd'
import './orders.less'

class Orders extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            isFlush: true
        };
    }

    componentDidMount() {
        this.props.getorderListAction();
    }

    deleteOrder(order) {
        // const _self = this;
        fetch('http://localhost:8080/api/deleteorder', {
            method: 'POST',
            body: JSON.stringify(order),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then( (response) => {    //如果使用function(response) ，就要在外面保存this值（_self）
            if (response.status === 200) {
                this.props.getorderListAction();
            }
        });

    }

    render() {
        // console.log(this.props.orderList || []);
        let datasource = [];
        // for (let noteIndex in this.props.orderList) {
        //     ul.push(<li key={noteIndex}>
        //         <p className='tb-head'>{this.props.orderList[noteIndex].productId.productName}</p>
        //         <p className='tb-head'>{this.props.orderList[noteIndex].productId.price}</p>
        //         <p className='tb-head'>{this.props.orderList[noteIndex].product_number}</p>
        //         <p className='tb-head'>{this.props.orderList[noteIndex].productId.unit}</p>
        //         <button>删除</button>
        //     </li>);
        // }
        this.props.orderList.map((item, index) => {
            datasource[index] = {
                key: index,
                name: item.productId.productName,
                price: item.productId.price,
                product_number: item.product_number,
                unit: item.productId.unit,
                operation: <Button onClick={this.deleteOrder.bind(this, item)}>删除</Button>
            }
        });

        const column = [
            {
                title: '名称',
                dataIndex: 'name',
                key: 'name',
            },
            {
                title: '单价',
                dataIndex: 'price',
                key: 'price',
            },
            {
                title: '数量',
                dataIndex: 'product_number',
                key: 'product_number',
            },
            {
                title: '单位',
                dataIndex: 'unit',
                key: 'unit',
            },
            {
                title: '操作',
                dataIndex: 'operation',
                key: 'operation',
            },
        ];


        return (
            <div className='orderList'>
                {/*<ul>*/}
                {/*    <li><p className='tb-head'>名称</p>*/}
                {/*        <p className='tb-head'>单价</p>*/}
                {/*        <p className='tb-head'>数量</p>*/}
                {/*        <p className='tb-head'>单位</p>*/}
                {/*        <p className='tb-head'>操作</p>*/}
                {/*    </li>*/}
                {/*    {ul}*/}
                {/*</ul>*/}
                {this.props.orderList.length!=0?
                    <Table dataSource={datasource} columns={column} className='table' />:
                    <Empty description="暂无订单，返回商城页面继续购买"/>}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    orderList: state.orderList.orderList
});
const mapDispatcherToProps = dispatch => bindActionCreators({
    getorderListAction
}, dispatch);
export default connect(mapStateToProps, mapDispatcherToProps)(Orders);