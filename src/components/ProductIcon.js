import React from 'react'
import {IoIosAddCircleOutline} from 'react-icons/io'
import {message} from 'antd'
import './ProductIcon.less'

class ProductIcon extends React.Component {


    constructor(props, context) {
        super(props, context);
        this.state = {
            order: {
                productId: this.props.product
            },
            isFinish:true
        }
    }

    addToOrders(e) {
        if(!this.state.isFinish){
            message.info('再等一等哦，订单要一个个创建～');
            return;
        }
        this.changeFinish.bind(this);
        fetch('http://localhost:8080/api/addproductToorderslist', {
            method: 'POST',
            body: JSON.stringify(this.state.order),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function (response) {
            if(response.status === 201){
                message.success('已添加到订单！')
            }else {
                message.warn('添加失败！')
            }
        });
        this.changeFinish.bind(this)
    }
    changeFinish(){
        this.setState((old)=>{
            isFinish:!old
        })
    }

    render() {
        return (
            <div className='details'>
                <img className='product-img' src={this.props.product.img}/>
                <p className='product-name'>{this.props.product.productName}</p>
                <p>Price:{this.props.product.price}RMB/{this.props.product.unit}</p>
                <IoIosAddCircleOutline size={30} onClick={this.addToOrders.bind(this)} className='add'/>
            </div>
        )
    }
};
export default ProductIcon