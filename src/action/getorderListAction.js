export const getorderListAction = () => (dispatch) => {
    fetch('http://localhost:8080/api/getallorders')
        .then(res => res.json())
        .then(res => {
            dispatch({
                type: 'GET_ORDER_LIST',
                payload: res
            })
        })
        .catch(error => {
            dispatch(fetchProductsError(error));
        })


};