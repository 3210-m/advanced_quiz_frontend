
export const getMallListAction = () => (dispatch) => {
    fetch('http://localhost:8080/api/getallproducts')
        .then(res => res.json())
        .then(res => {
            dispatch({
                type: 'GET_MALL_LIST',
                payload: res
            })
        })
        .catch(error => {
            dispatch(fetchProductsError(error));
        })


};