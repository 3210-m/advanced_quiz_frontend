const init = {
    orderList: []
};


export default (state = init, action) => {
    if ('GET_ORDER_LIST' === action.type) {
        return {
            ...state,
            orderList: action.payload
        };
    } else {
        return state
    }
};