import {combineReducers} from "redux";
import orderListReducer from "./orderListReducer";
import mallReducer from "./mallReducer";


const reducers = combineReducers({
    orderList:orderListReducer,
    mallList:mallReducer
});
export default reducers;