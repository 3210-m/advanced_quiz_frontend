const init = {
    mallList: []
};


export default (state = init, action) => {
    if ('GET_MALL_LIST' === action.type) {
        return {
            ...state,
            mallList: action.payload
        };
    } else {
        return state
    }
};